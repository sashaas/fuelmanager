﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public class CurrencyType : EntityBase
	{
		[Required]
		public string Name { get; set; }

		public string Country { get; set; }

		public string Description { get; set; }

		public virtual ICollection<FuelExpense> Expenses { get; set; }
		public virtual ICollection<UserSettings> SettingsForCurrency { get; set; }
	}
}
