﻿using System.ComponentModel.DataAnnotations;

namespace FuelManager.Common.Models
{
	public class LoginModel
	{
		[Required(ErrorMessage = "User name is required.")]
		public string Username { get; set; }

		[Required(ErrorMessage = "The password is required.")]
		public string Password { get; set; }

		public bool RememberMe { get; set; }
	}
}
