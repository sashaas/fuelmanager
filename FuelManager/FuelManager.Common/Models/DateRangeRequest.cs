﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FuelManager.Common
{
	public class DateRangeRequest
	{
		[Required]
		public DateTime From { get; set; }
		[Required]
		public DateTime To { get; set; }
	}
}
