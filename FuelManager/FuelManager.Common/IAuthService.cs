﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public enum AuthStatus
	{
		None = 0,
		Ok,
		Error,
		UserNotFound,
		InvalidPassword,
		UserWithSuchNameExists
	}

	public class AuthResponse
	{
		public Exception Error { get; set; }
		public AuthStatus Status { get; set; }

		public static AuthResponse CreateOk() { return new AuthResponse { Status = AuthStatus.Ok }; }
		public static AuthResponse CreateUserNotFound() { return new AuthResponse { Status = AuthStatus.UserNotFound }; }
		public static AuthResponse CreateUserExists() { return new AuthResponse { Status = AuthStatus.UserWithSuchNameExists }; }
		public static AuthResponse CreateError(Exception ex) { return new AuthResponse { Status = AuthStatus.Error, Error = ex }; }
		public static AuthResponse CreateInvalidPassword() { return new AuthResponse { Status = AuthStatus.InvalidPassword }; }
	}

	public interface IAuthService
	{
		Task<AuthResponse> AuthenticateUser(string name, string pass);
		Task<AuthResponse> Logout(string name);
		Task<AuthResponse> Register(string name, string password, string email);
	}
}
