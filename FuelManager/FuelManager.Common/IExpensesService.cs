﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public interface IExpensesService
	{
		Task<List<FuelExpense>> GetPaged(PagedRequest req);
		Task<List<FuelExpense>> GetAll();

		Task Add(FuelExpense expense, int userId);
		Task Remove(FuelExpense expense, int userId);
		Task Update(FuelExpense expense, int userId);
	}
}
