﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public interface IUserService
	{
		Task<string> GetUserRole(string username);

		Task<UserSettings> GetSettings(string username);

		Task UpdateSettingsCurrency(string username, CurrencyType newCurrency);

		Task ChangeUserPassword(string uname,string oldPassword, string newPassword);
	}
}
