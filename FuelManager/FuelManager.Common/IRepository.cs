﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public class PagedRequest
	{
		public int Page { get; set; }
		public int ItemsPerPage { get; set; }
	}

	public interface IRepository<T> where T : EntityBase
	{
		int Count { get; }

		IEnumerable<T> GetAll();
		IEnumerable<T> GetPaged(PagedRequest request);
		IQueryable<T> GetBy(Expression<Func<T, bool>> exp);

		Task<T> GetById(int id);

		void Add(T entity);
		void Update(T entity);
		void Delete(T entity);
	}
}
