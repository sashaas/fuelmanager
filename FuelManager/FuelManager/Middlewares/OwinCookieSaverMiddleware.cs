﻿using Microsoft.Owin;
using Owin;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.Middlewares
{
	/// <summary>
	/// Extension method for registering middleware.
	/// </summary>
	public static class OwinCookieSaverExtensions
	{
		/// <summary>
		/// Enables the Owin Cookie Saver middleware, that prevents owin
		/// auth cookies from disappearing. Add above any cookie middleware.
		/// </summary>
		/// <param name="app">Owin app</param>
		/// <returns>Owin app</returns>
		public static IAppBuilder UseOwinCookieSaver(this IAppBuilder app)
		{
			app.Use(typeof(OwinCookieSaverMiddleware));
			return app;
		}
	}

	/// <summary>
	/// Cookie saving middleware.
	/// </summary>
	public class OwinCookieSaverMiddleware : OwinMiddleware
	{
		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="next">Next middleware in chain</param>
		public OwinCookieSaverMiddleware(OwinMiddleware next)
			: base(next) { }

		static PropertyInfo fromHeaderProperty = typeof(HttpCookie).GetProperty(
			"FromHeader", BindingFlags.NonPublic | BindingFlags.Instance);

		/// <summary>
		/// Main entry point of middleware.
		/// </summary>
		/// <param name="context">Owin Context</param>
		/// <returns>Task</returns>
		public async override Task Invoke(IOwinContext context)
		{
			await Next.Invoke(context);

			var setCookie = context.Response.Headers.GetValues("Set-Cookie");
			if (setCookie != null)
			{
				var cookies = FuelManager.Infrastructure.CookieParser.Parse(setCookie);

				foreach (var c in cookies)
				{
					// Guard for stability, preventing nullref exception in case private reflection breaks.
					if (fromHeaderProperty != null)
					{
						// Mark the cookie as coming from a header, to prevent System.Web from adding it again.
						fromHeaderProperty.SetValue(c, true);
					}

					// HttpContext.Current turns to null in some cases after the
					// async call to Next.Invoke() when run in a web forms
					// application. Let's grab it from the owin environment instead.
					var httpContext = context.Get<HttpContextBase>(typeof(HttpContextBase).FullName);

					if (httpContext != null && !httpContext.Response.Cookies.AllKeys.Contains(c.Name))
					{
						httpContext.Response.Cookies.Add(c);
					}
				}
			}
		}
	}
}