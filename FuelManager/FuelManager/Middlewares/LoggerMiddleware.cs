﻿using Microsoft.Owin;
using System.Threading.Tasks;

namespace FuelManager.Middlewares
{
	public class RequestLoggerMiddleware : OwinMiddleware
	{
		private readonly log4net.ILog _logger;

		public RequestLoggerMiddleware(OwinMiddleware next, log4net.ILog logger)
			: base(next)
		{
			_logger = logger;
		}

		public override async Task Invoke(IOwinContext context)
		{
			string authHeader = string.Empty;
			var method = string.Empty;
			var path = string.Empty;
			var statusCode = string.Empty;

			//Before Katana inline middleware
			// Request
			if (context.Request != null)
			{
				method = context.Request.Method;
				if (context.Request.Headers.Count > 0)
				{
					authHeader = context.Request.Headers.Get("Authorization");
				}
				path = context.Request.Path.ToString();
			}

			if (string.IsNullOrEmpty(authHeader))
				_logger.Debug(string.Format("BEFORE METHOD: {0} - PATH: {1}", method, path));
			else
				_logger.Debug(string.Format("BEFORE METHOD: {0} - PATH: {1} - AUTH : {2}", method, path, authHeader));

			// Invoking everything else
			await Next.Invoke(context);
			
			//After Katana inline middleware
			// Response
			if (context.Response != null)
			{
				statusCode = context.Response.StatusCode.ToString();
			}

			_logger.Debug(string.Format("AFTER METHOD: {0} - PATH: {1} - STATUSCODE: {2}", method, path, statusCode));
		}
	}
}