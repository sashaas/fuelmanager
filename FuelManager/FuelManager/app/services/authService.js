﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.factory('authService', ['$http', '$q', '$window', function ($http, $q, $window, ngAuthSettings) {

		var authServiceFactory = {};

		var _authentication = {
			isAuth: false,
			username: "",
			token: ""
		};

		var _externalAuthData = {
			provider: "",
			username: "",
			externalAccessToken: ""
		};

		var _doRegistration = function (registration) {

			_logOut();

			var deferred = $q.defer();

			$http({
				method: "POST",
				url: "/api/auth/register",
				data: registration,
				headers: { 'Content-Type': 'application/json' }
			}).success(function (response, status, headers, config) {

				_authentication.isAuth = true;
				_authentication.username = username;

				deferred.resolve(response, status, headers, config);
			}).error(function (error) {
				deferred.reject(error);
				resetAuth();
			});

			return deferred.promise;
		};

		var _login = function (loginData) {

			var deferred = $q.defer();
			var username = loginData.username;

			$http({
				method: "POST",
				url: "/api/auth/login",
				data: loginData,
				headers: { 'Content-Type': 'application/json' }
			}).success(function (response, status, headers, config) {

				_authentication.isAuth = true;
				_authentication.username = username;
				_authentication.token = response.token;

				if (!$window.sessionStorage["authData"]) {
					$window.sessionStorage["authData"] = JSON.stringify(_authentication);
				}
				else {
					$window.sessionStorage["authData"] = null;
					$window.sessionStorage["authData"] = JSON.stringify(_authentication);
				}

				deferred.resolve(response, status, headers, config);
			}).error(function (error) {

				resetAuth();

				if ($window.sessionStorage["authData"]) {
					$window.sessionStorage.clear();
				}
				deferred.reject(error);
			});

			return deferred.promise;
		};

		var _logOut = function () {

			$window.sessionStorage.clear();

			resetAuth();

			var deferred = $q.defer();

			$http({
				method: "POST",
				url: "/api/auth/logout",
				data: null,
				headers: { 'Content-Type': 'application/json' }
			}).success(function (response, status, headers, config) {

				deferred.resolve(response, status, headers, config);
			}).error(function (error) {
				console.error(error);
				deferred.reject(error);
			});
		};

		var _fillAuthData = function () {

			if ($window.sessionStorage["authData"]) {
				var jsData = $window.sessionStorage["authData"];
				if (jsData === null) {
					resetAuth();
					return;
				}
				console.log(jsData);
				var data = JSON.parse(jsData);
				_authentication.isAuth = data.isAuth;
				_authentication.username = data.username;
				_authentication.token = data.token;
			}
			else {
				resetAuth();
			}
		};

		function resetAuth() {
			_authentication.isAuth = false;
			_authentication.username = "";
			_authentication.token = "";
		};

		var _registerExternal = function (registerExternalData) {

			var deferred = $q.defer();

			$http.post(serviceBase + 'api/account/registerexternal', registerExternalData).success(function (response) {

				$window.localStorage.setItem('authData', { token: response.access_token, username: response.username, refreshToken: "", useRefreshTokens: false });

				_authentication.isAuth = true;
				_authentication.username = response.username;

				deferred.resolve(response);

			}).error(function (err, status) {
				_logOut();
				deferred.reject(err);
			});

			return deferred.promise;

		};

		authServiceFactory.saveRegistration = _doRegistration;
		authServiceFactory.login = _login;
		authServiceFactory.logOut = _logOut;
		authServiceFactory.fillAuthData = _fillAuthData;
		authServiceFactory.authentication = _authentication;

		authServiceFactory.externalAuthData = _externalAuthData;
		authServiceFactory.registerExternal = _registerExternal;

		return authServiceFactory;

	}]);
})();