﻿'use strict';

var app = angular.module('app');

app.factory('authInterceptorService', ['$rootScope', '$q', '$location', '$window', '$injector', function ($rootScope, $q, $location, $window, $injector) {

	var authInterceptorServiceFactory = {};

	var _request = function (config) {

		config.headers = config.headers || {};

		if ($window.sessionStorage["authData"]) {
			var json = JSON.parse($window.sessionStorage["authData"]);
			config.headers.Authorization = 'Bearer ' + json.token;
		}

		return config;
	}

	var _responseError = function (response) {
		if (response.status === 401) {

			if ($window.sessionStorage["authData"]) {
				$window.sessionStorage.clear();
			}
			var authService = $injector.get('authService');
			authService.fillAuthData();
			console.warn("Redirecting to the login page due to the HTTP 401.");
			$location.path('/login');
		}

		return $q.reject(response);
	}

	authInterceptorServiceFactory.request = _request;
	authInterceptorServiceFactory.responseError = _responseError;

	return authInterceptorServiceFactory;
}]);