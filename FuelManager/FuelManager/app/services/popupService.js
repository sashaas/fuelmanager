﻿'use strict';

var app = angular.module('app');

app.factory('popupService', ['$window', function ($window) {

	var popupServiceFactory = {};

	var _getOrders = function () {

		return $http.get(serviceBase + 'api/orders').then(function (results) {
			return results;
		});
	};

	var _showPopup = function (message) {
		return $window.confirm(message);
	}

	popupServiceFactory.showPopup = _showPopup;

	return popupServiceFactory;

}]);