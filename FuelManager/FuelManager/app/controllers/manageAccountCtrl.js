﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.controller('manageAccountCtrl', ['$scope', '$http', function ($scope, $http) {

		$scope.title = "Manage your account here";
		$scope.doPassword = true;
		$scope.errorMsg = '';
		$scope.successMsg = '';

		var timerHandler = null;

		$scope.changePassword = {
			oldPassword: '',
			password: '',
			confirmPassword: ''
		};

		$scope.showPasswordForm = function () {

			$scope.doPassword = false;
			$scope.errorMsg = '';
			$scope.successMsg = '';
		}

		$scope.doChangePassword = function (isValid, data) {

			if (!isValid)
				return;

			$http({
				method: "POST",
				url: "/api/auth/changepassword",
				data: data,
				headers: { 'Content-Type': 'application/json' }
			}).success(function (response, status, headers, config) {

				$scope.doPassword = true;
				$scope.errorMsg = '';
				$scope.changePassword.oldPassword = '';
				$scope.changePassword.password = '';
				$scope.changePassword.confirmPassword = '';
				$scope.successMsg = 'Password has been successfully changed.';

				timerHandler = setInterval(function () {
					$scope.$apply(function () {
						$scope.successMsg = '';
					});
					
					console.info("performed!!!");
					clearInterval(timerHandler);
					timerHandler = null;
				}, 3000);

			}).error(function (error) {
				if (error.exceptionMessage != undefined) {
					$scope.errorMsg = error.exceptionMessage;
				}
				else
					$scope.errorMsg = error.message;
				$scope.doPassword = false;
			});
		}

	}]);

})();