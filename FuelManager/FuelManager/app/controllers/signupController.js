﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.controller('signupController', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {

		$scope.registeredSuccessfully = false;
		$scope.message = "";

		$scope.registration = {
			userName: "",
			password: "",
			confirmPassword: "",
			email: ""
		};

		$scope.signUp = function () {

			authService.saveRegistration($scope.registration).then(function (response) {

				$scope.registeredSuccessfully = true;
				$scope.message = "User has been registered successfully, redirecting to the login page(in 2 seconds).";
				startRedirection();

			},
			 function (response) {
			 	var errors = [];
			 	for (var key in response.data.modelState) {
			 		for (var i = 0; i < response.data.modelState[key].length; i++) {
			 			errors.push(response.data.modelState[key][i]);
			 		}
			 	}
			 	$scope.message = "Failed to register user due to:" + errors.join(' ');
			 });
		};

		var startRedirection = function () {
			var timer = $timeout(function () {
				$timeout.cancel(timer);
				$location.path('/login');
			}, 2000);
		}

	}]);
})();