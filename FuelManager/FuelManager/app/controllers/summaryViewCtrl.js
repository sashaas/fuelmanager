﻿(function myfunction() {
	'use strict';

	var app = angular.module('app');

	app.controller('summaryViewCtrl', ['$scope', '$filter', '$http', function myfunction($scope, $filter, $http) {

		$scope.title = "Summary reporting";
		$scope.isUploaded = false;
		$scope.error = "";
		$scope.isLoaded = false;

		$scope.dateRange = {
			from: undefined,
			to: undefined
		};

		$scope.summary = {};

		$scope.submitForm = function (isValid) {
			$scope.isUploaded = true;
			$scope.isLoaded = false;

			var dateValue1 = $filter('date')($scope.dateRange.from, 'MM.dd.yyyy');
			var dateValue2 = $filter('date')($scope.dateRange.to, 'MM.dd.yyyy');

			var range = {
				From: dateValue1,
				To: dateValue2
			};

			console.info(range);

			$http({
				method: "POST",
				url: "/api/summary/getsummary",
				data: range,
				headers: { 'Content-Type': 'application/json' }
			}).success(function (response, status, headers, config) {
				$scope.summary = response;
				$scope.isLoaded = true;

			}).error(function (error) {
				$scope.error = error;
				console.warn(error);
				$scope.isLoaded = false;
			});
		};

		$scope.saveReport = function (isvalid, sum) {
			if (!isvalid)
				return;

			$http({
				method: "POST",
				url: "/api/summary/SaveSummary",
				data: sum,
				headers: { 'Content-Type': 'application/json' }
			}).success(function (response, status, headers, config) {
				console.info("Done");

			}).error(function (error) {
				$scope.error = error;
			});
		}

	}]);

})();