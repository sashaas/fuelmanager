﻿(function () {
	'use strict';

	var controllerId = 'expensesController';
	var app = angular.module('app');

	//app.directive('expense-partial', function () {
	//	return {
	//		templateUrl: 'ExpensePartial.html'
	//	};
	//});

	app.controller(controllerId, ['$scope', '$http', 'crudService', 'popupService', function ($scope, $http, crudService, popupService) {

		$scope.title = "Manage your expenses here.";

		$scope.expenses = [];
		$scope.isLoading = true;
		$scope.errorMsg = '';

		$scope.delete = function (indx) {
			var rez = popupService.showPopup('Are you sure?');
			if (rez) {
				var expense = $scope.expenses[indx];
				var promise = crudService.delete(expense.id);

				promise.then(function (data) {
					$scope.expenses.splice(indx, 1); // remove 1 element starting at index [indx]
				},
				function (error) {
				});
			}
		};

		var promise = crudService.getExpenses();

		promise.then(
		function (payload) {
			$scope.expenses = payload.data;
			$scope.isLoading = false;
			$scope.errorMsg = '';
		},
		function (error) {
			
			$scope.isLoading = false;
			$scope.errorMsg = error.statusText;
			$scope.expenses = [];
		});
	}]);

})();