﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.controller('userSettingsCtrl', ['$scope', '$location', '$http', 'authService', function ($scope, $location, $http, authService) {


		$scope.title = "Manage your settings here.";
		$scope.errorMsg = "";
		$scope.updateError = "";

		$scope.settings = {};
		$scope.selectedCurrency = {};
		$scope.currencies = [];

		$scope.isSelected = false;

		$http({
			method: "GET",
			url: "/api/user/GetSettings/",
			headers: { 'Content-Type': 'application/json' }
		}).success(function (response, status, headers, config) {
			$scope.settings = response;
		}).error(function (error) {
			$scope.errorMsg = error;
		});

		$http({
			method: "GET",
			url: "/api/Currency/GetAll/",
			headers: { 'Content-Type': 'application/json' }
		}).success(function (response, status, headers, config) {
			$scope.currencies = response;
		}).error(function (error) {
			$scope.errorMsg = error;
		});


		$scope.updateCurrency = function () {

			if ($scope.selectedCurrency != undefined) {
				
				$http({
					method: "POST",
					url: "/api/User/UpdateUserCurrency/",
					data: $scope.selectedCurrency,
					headers: { 'Content-Type': 'application/json' }
				}).success(function (response, status, headers, config) {
					$scope.settings.currencyName = $scope.selectedCurrency.name;
				}).error(function (error) {
					$scope.updateError = error;
				});
			}
		};

		$scope.selectAction = function () {
			console.info($scope.selectedCurrency);
			if ($scope.selectedCurrency != undefined)
				$scope.isSelected = true;
		};

	}]);

})();