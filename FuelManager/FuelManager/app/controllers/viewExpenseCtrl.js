﻿(function () {
	'use strict';

	var app = angular.module('app');

	var controllerId = 'viewExpenseCtrl';
	app.controller(controllerId, ['$scope', '$http', '$routeParams', 'crudService', function ($scope, $http, $routeParams, crudService) {

		$scope.title = 'View Expense.';
		$scope.isUploaded = false;
		

		console.log($routeParams.id);

		var promise = crudService.getExpenseById($routeParams.id);
		promise.then(
					function (payload) {
						$scope.expense = payload;
						console.log('failure loading movie', payload);
					},
					function (error) {
						$scope.isUploaded = false;
						console.log('failure loading movie', error);
					});

	}]);
})();