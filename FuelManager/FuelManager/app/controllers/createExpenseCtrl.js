﻿(function () {
	'use strict';

	var controllerId = 'createExpenseCtrl';
	angular.module('app')
        .controller(controllerId, ['$scope', '$http', 'crudService', function ($scope, $http, crudService) {

        	$scope.title = 'Create a new expense.';
        	$scope.isUploaded = false;

        	$scope.model = {};

        	$scope.model.date = "";
        	$scope.model.litres;
        	$scope.model.overallPrice;
        	$scope.model.mileageBeforeRefuel;
        	$scope.model.gasStation = "";
        	$scope.model.pricePerLiter;

        	$scope.clear = function () {
        		$scope.model.date = undefined;
        		$scope.model.litres = 0;
        		$scope.model.overallPrice = 0;
        		$scope.model.mileageBeforeRefuel = 0;

        		$scope.model.gasStation = "";
        		$scope.model.pricePerLiter = 0.0;
        	};

        	$scope.calculate = function () {
        		var litres = $scope.model.litres;
        		var price = $scope.model.pricePerLiter;

        		if (litres == undefined || price == undefined)
        			return;

        		if (litres > 0 && price > 0) {
        			$scope.model.overallPrice = litres * price;
        		}
        	};

        	$scope.submitForm = function (isValid) {
        		$scope.isUploaded = true;
        		var promise = crudService.post($scope.model);
        		promise.then(
					function (payload) {
						$scope.isUploaded = false;
					},
					function (error) {
						$scope.isUploaded = false;
					});
        	};
        }]);
})();