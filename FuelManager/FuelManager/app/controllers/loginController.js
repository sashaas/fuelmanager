﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.controller('loginController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {

		$scope.loginData = {
			username: "",
			password: "",
			rememberMe: false
		};

		$scope.message = "";

		$scope.login = function () {

			authService.login($scope.loginData).then(
			function (response) {
				$location.path('/expenses');
			},
			function (error) {

				var msg = "";
				if (error.modelState != undefined) {
					var state = error.modelState;
					state.username != undefined;
					msg += msg + state.username;
				}

				console.warn(error);
				if (error.message != undefined)
					$scope.message = error.message;
				else
					$scope.message = error.toString();

				var state = error.modelState;
			});
		};
	}]);
})();