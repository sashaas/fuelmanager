﻿using Hangfire.Common;
using Hangfire.States;
using Hangfire.Storage;

namespace FuelManager.Infrastructure.Attributes
{
	public class LogFailureAttribute
	{
		public class LogFailure : JobFilterAttribute, IApplyStateFilter
		{
			public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
			{
				var failedState = context.NewState as FailedState;
				if (failedState != null)
				{
					GlobalShare.Instance.Logger.Error(string.Format("Background job #{0} was failed with an exception.", context.JobId),
						failedState.Exception);
				}
			}

			public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
			{
			}
		}
	}
}