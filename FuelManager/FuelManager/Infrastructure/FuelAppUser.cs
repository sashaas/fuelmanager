﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace FuelManager.Infrastructure
{
	public class FuelAppUser : ClaimsPrincipal
	{
		public FuelAppUser(ClaimsPrincipal principal)
			: base(principal)
		{
		}

		public string Name
		{
			get
			{
				return this.FindFirst(ClaimTypes.Name).Value;
			}
		}

		public string Role
		{
			get
			{
				return this.FindFirst(ClaimTypes.Role).Value;
			}
		}

	}
}