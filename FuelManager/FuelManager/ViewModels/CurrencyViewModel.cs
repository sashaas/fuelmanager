﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FuelManager.ViewModels
{
	public class CurrencyViewModel
	{
		[Required]
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
	}
}