﻿using System.ComponentModel.DataAnnotations;

namespace FuelManager.ViewModels
{
	public class JwtTokenResponse
	{
		[Required]
		public string Token { get; set; }
	}
}