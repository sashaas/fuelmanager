﻿using log4net;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager
{
	public class GlobalShare
	{
		private static Lazy<GlobalShare> _instance = new Lazy<GlobalShare>(() => new GlobalShare());

		private GlobalShare()
		{
			log4net.Config.XmlConfigurator.Configure();
			Logger = LogManager.GetLogger(typeof(GlobalShare));

			Kernel = CreateKernel().Result;
		}

		public static GlobalShare Instance
		{
			get
			{
				return _instance.Value;
			}
		}

		public StandardKernel Kernel { get; set; }
		public ILog Logger { get; set; }


		public async Task<StandardKernel> CreateKernel()
		{
			var kernel = new StandardKernel();
			kernel.Load(System.Reflection.Assembly.GetExecutingAssembly());
			kernel.Load("FuelManager.Common");

			Infrastructure.ConfigManager confMan = new Infrastructure.ConfigManager();
			Infrastructure.ConfigData data = await confMan.GetConfiguration();
			if (data != null && data.Config.Services != null && data.Config.Services.Count > 0)
			{
				foreach (var service in data.Config.Services)
				{
					if (!service.Singleton)
						kernel.Bind(System.Type.GetType(service.Contract)).To(System.Type.GetType(service.Implementation));
					else
						kernel.Bind(System.Type.GetType(service.Contract)).To(System.Type.GetType(service.Implementation)).InSingletonScope();
				}
			}

			kernel.Bind<ILog>().ToMethod((ctx) => GlobalShare.Instance.Logger);

			return kernel;
		}
	}
}