﻿using log4net;
using Ninject;
using System;
using System.Linq;

namespace FuelManager
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			//SetupGlobals(GlobalShare.Instance.Logger, GlobalShare.Instance.Kernel);
		}

		protected void Application_End(object sender, EventArgs e)
		{
			log4net.LogManager.Shutdown();
		}
	}
}