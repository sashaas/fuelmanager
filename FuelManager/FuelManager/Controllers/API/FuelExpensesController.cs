﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace FuelManager.Controllers.API
{
	[Authorize]
	public class FuelExpensesController : AuthApiBase
	{
		private readonly IUnitOfWorkFactory _factory;
		private readonly IExpensesService _service;

		public FuelExpensesController(IUnitOfWorkFactory factory, IExpensesService service)
		{
			_factory = factory;
			_service = service;
		}

		// GET api/<controller>
		[HttpGet]
		[ResponseType(typeof(List<FuelExpense>))]
		public async Task<IHttpActionResult> Get()
		{
			return Ok(await _service.GetAll());
		}

		// GET api/<controller>/view/5
		[HttpGet]
		[ResponseType(typeof(FuelExpense))]
		public async Task<IHttpActionResult> View(int id)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.ReadOnly))
			{
				var repo = uow.Getrepository<FuelExpense>();
				FuelExpense expense = await repo.GetById(id);
				if (expense != null)
					return Ok<FuelExpense>(expense);
				return NotFound();
			}
		}

		[HttpPost]
		[ResponseType(typeof(FuelExpense))]
		public async Task<IHttpActionResult> Create(FuelExpense expense)
		{
			if (expense == null)
				return BadRequest();

			try
			{
				User user = await GetUser();
				await _service.Add(expense, user.Id);
			}
			catch (Exception ex) { return InternalServerError(ex); }

			return Ok<FuelExpense>(expense);
		}

		[HttpDelete]
		public async Task<IHttpActionResult> Delete(int id)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.Full))
			{
				var repo = uow.Getrepository<FuelExpense>();
				FuelExpense expense = await repo.GetById(id);
				if (expense != null)
				{
					repo.Delete(expense);
					await uow.SaveChanges();
					return Ok();
				}

				return NotFound();
			}
		}
	}
}