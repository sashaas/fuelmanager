﻿using FuelManager.Common;
using FuelManager.ViewModels;
using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace FuelManager.Controllers.API
{
	[Authorize]
	public class UserController : AuthApiBase
	{
		private readonly IUserService _userSvc;

		public UserController(IUserService svc)
		{
			_userSvc = svc;
		}

		[HttpGet]
		[ResponseType(typeof(UserSettingsViewModel))]
		public async Task<IHttpActionResult> GetSettings()
		{
			UserSettingsViewModel vm = null;
			try
			{
				Claim claim = AuthenticationManager.User.FindFirst(ClaimTypes.Name);
				UserSettings data = await _userSvc.GetSettings(claim.Value);

				vm = new UserSettingsViewModel();
				vm.CurrencyName = data.SelctedCurrency.Name;
				vm.CurrencyId = data.SelctedCurrency.Id;
			}
			catch (Exception ex)
			{
				return InternalServerError(ex);
			}

			return Ok(vm);
		}

		[HttpPost]
		public async Task<IHttpActionResult> UpdateUserCurrency(CurrencyViewModel model)
		{
			try
			{
				Claim claim = AuthenticationManager.User.FindFirst(ClaimTypes.Name);

				CurrencyType newCur = new CurrencyType() { Id = model.Id, Name = model.Name };
				await _userSvc.UpdateSettingsCurrency(claim.Value, newCur);
			}
			catch (Exception ex)
			{
				return InternalServerError(ex);
			}

			return Ok();
		}

	}
}