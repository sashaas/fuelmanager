﻿using FuelManager.Common;
using FuelManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace FuelManager.Controllers.API
{
	[Authorize]
	public class CurrencyController : AuthApiBase
	{
		private readonly IUnitOfWorkFactory _factory;

		public CurrencyController(IUnitOfWorkFactory fact)
		{
			_factory = fact;
		}

		[HttpGet]
		[ResponseType(typeof(List<CurrencyViewModel>))]
		public async Task<IHttpActionResult> GetAll()
		{
			try
			{
				using (IUnitOfWork uow = _factory.Create(UnitType.ReadOnly))
				{
					var repo = uow.Getrepository<CurrencyType>();

					var result = repo.GetAll().Select(x => new CurrencyViewModel() { Id = x.Id, Name = x.Name }).ToList();
					return Ok( await Task.FromResult<List<CurrencyViewModel>>(result));
				}
			}
			catch (Exception ex)
			{
				return InternalServerError(ex);
			}
		}
	}
}