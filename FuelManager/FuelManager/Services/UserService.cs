﻿using FuelManager.Common;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FuelManager.Services
{
	public class UserService : IUserService
	{
		private readonly IUnitOfWorkFactory _factory;
		private readonly ICryptoService _crypto;

		public UserService(IUnitOfWorkFactory factory, ICryptoService crypt)
		{
			_factory = factory;
			_crypto = crypt;
		}

		public async Task<string> GetUserRole(string username)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.ReadOnly))
			{
				User user = await uow.Getrepository<User>().GetBy(x => x.Username == username).Include("Roles").FirstOrDefaultAsync();
				if (user != null)
					return string.Join(",", user.Roles.Select(x => x.Name));
				else
					throw new InvalidOperationException("No such user has been found.");
			}
		}

		public async Task<UserSettings> GetSettings(string username)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.ReadOnly))
			{
				User user = await uow.Getrepository<User>().GetBy(x => x.Username == username).Include("Settings").FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				UserSettings settings = await uow.Getrepository<UserSettings>().GetBy(x => x.Id == user.Settings.Id).Include("SelctedCurrency").FirstOrDefaultAsync();
				if (settings == null)
					throw new InvalidOperationException("User has no settings defined.");
				return settings;
			}
		}

		public async Task UpdateSettingsCurrency(string username, CurrencyType newCurrency)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.Full))
			{
				User user = await uow.Getrepository<User>().GetBy(x => x.Username == username).Include("Settings").FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				UserSettings settings = await uow.Getrepository<UserSettings>().GetBy(x => x.Id == user.Settings.Id).Include("SelctedCurrency").FirstOrDefaultAsync();
				if (settings == null)
					throw new InvalidOperationException("User has no settings defined.");

				CurrencyType newT = await uow.Getrepository<CurrencyType>().GetBy(x => x.Id == newCurrency.Id).FirstOrDefaultAsync();


				settings.CurrencyId = newT.Id;
				settings.SelctedCurrency = newT;
				uow.Getrepository<UserSettings>().Update(settings);

				await uow.SaveChanges();
			}
		}


		public async Task ChangeUserPassword(string uname, string oldPassword, string newPassword)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.Full))
			{
				IRepository<User> userRepo=uow.Getrepository<User>();
				User user = await userRepo.GetBy(x => x.Username == uname).FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				string oldCrypted= await _crypto.Encrypt(oldPassword, ConfigurationManager.AppSettings["Security:Phrase"]);
				string newCrypted= await _crypto.Encrypt(newPassword, ConfigurationManager.AppSettings["Security:Phrase"]);
				if (user.Password != oldCrypted)
					throw new InvalidOperationException("Old password is not correct.");

				user.Password = newCrypted;
				userRepo.Update(user);

				await uow.SaveChanges();
			}
		}
	}
}