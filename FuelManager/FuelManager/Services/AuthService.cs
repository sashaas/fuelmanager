﻿using FuelManager.Common;
using FuelManager.DataAccess;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
namespace FuelManager.Services
{
	public class AuthService : IAuthService
	{
		private readonly IUnitOfWorkFactory _fact;
		private readonly ICryptoService _crypto;

		public AuthService(IUnitOfWorkFactory factory, ICryptoService crypto)
		{
			_fact = factory;
			_crypto = crypto;
		}

		public async Task<AuthResponse> AuthenticateUser(string name, string pass)
		{
			using (IUnitOfWork uow = _fact.Create(UnitType.ReadOnly))
			{
				pass = await _crypto.Encrypt(pass, ConfigurationManager.AppSettings["Security:Phrase"]);
				IRepository<User> userRepo = uow.Getrepository<User>();
				var user = await userRepo.GetBy(x => x.Username == name).FirstOrDefaultAsync();
				if (user == null)
					return AuthResponse.CreateUserNotFound();

				if (!user.Password.Equals(pass, StringComparison.OrdinalIgnoreCase))
					return AuthResponse.CreateInvalidPassword();
			}
			return AuthResponse.CreateOk();
		}

		public async Task<AuthResponse> Logout(string uname)
		{
			using (IUnitOfWork uow = _fact.Create(UnitType.Full))
			{
				IRepository<User> userRepo = uow.Getrepository<User>();
				IRepository<UserInfo> userInfoRepo = uow.Getrepository<UserInfo>();

				var user = await userRepo.GetBy(x => x.Username == uname).Include("UserInfo").FirstOrDefaultAsync();
				if (user != null)
				{
					user.UserInfo.LastAccess = DateTime.Now;
				}

				userInfoRepo.Update(user.UserInfo);
				await uow.SaveChanges();
			}

			return AuthResponse.CreateOk();
		}

		public async Task<AuthResponse> Register(string name, string password, string email)
		{
			using (IUnitOfWork uow = _fact.Create(UnitType.Full))
			{
				IRepository<User> userRepo = uow.Getrepository<User>();
				IRepository<UserInfo> userInfoRepo = uow.Getrepository<UserInfo>();
				IRepository<Role> roleRepo = uow.Getrepository<Role>();
				var user = await userRepo.GetBy(x => x.Username == name).FirstOrDefaultAsync();
				if (user != null)
					return AuthResponse.CreateUserExists();

				User usr = new User()
				{
					Username = name,
					Email = email,
					UserInfo = new UserInfo { LastAccess = DateTime.Now }
				};
				usr.Password = await _crypto.Encrypt(password, ConfigurationManager.AppSettings["Security:Phrase"]);

				userRepo.Add(usr);

				Role role = await roleRepo.GetBy(x => x.Name.Contains("System")).FirstOrDefaultAsync();
				if (role != null)
				{
					usr.Roles = new List<Role>() { role };
				}
				roleRepo.Update(role);
				await uow.SaveChanges();
			}

			return await Task.FromResult<AuthResponse>(AuthResponse.CreateOk());
		}
	}
}