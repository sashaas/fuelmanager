﻿using FuelManager.Common;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.Services
{
	public class ExpensesService : IExpensesService
	{
		readonly IUnitOfWorkFactory _factory;

		public ExpensesService(IUnitOfWorkFactory fact)
		{
			_factory = fact;
		}

		public async Task<List<Common.FuelExpense>> GetPaged(PagedRequest request)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.ReadOnly))
			{
				var repo = uow.Getrepository<FuelExpense>();
				int totalPages = (int)Math.Ceiling((double)repo.Count / request.ItemsPerPage);
				int skip = (request.Page - 1) * request.ItemsPerPage;
				var data = repo.GetBy(x => x.Id > 0).Skip(skip).Take(request.ItemsPerPage).ToList();
				return await Task.FromResult<List<Common.FuelExpense>>(data);
			}
		}

		public async Task<List<Common.FuelExpense>> GetAll()
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.ReadOnly))
			{
				var repo = uow.Getrepository<FuelExpense>();
				var data = repo.GetAll().ToList();
				return await Task.FromResult<List<Common.FuelExpense>>(data);
			}
		}

		public async Task Add(Common.FuelExpense expense, int userId)
		{
			using (IUnitOfWork uow = _factory.Create(UnitType.Full))
			{
				var repo = uow.Getrepository<FuelExpense>();
				var userRepo = uow.Getrepository<User>();
				var settingsRepo = uow.Getrepository<UserSettings>();

				var user = await userRepo.GetById(userId);
				if (user == null)
					throw new InvalidOperationException("No such user was found.");

				expense.Owner = user;
				var setting = await settingsRepo.GetBy(x => x.Id == user.Id).Include(x => x.SelctedCurrency).FirstOrDefaultAsync();
				expense.CurrencyTypeId = setting.CurrencyId;

				repo.Add(expense);
				await uow.SaveChanges();
			}
		}

		public Task Remove(Common.FuelExpense expense, int userId)
		{
			throw new NotImplementedException();
		}

		public Task Update(Common.FuelExpense expense, int userId)
		{
			throw new NotImplementedException();
		}
	}
}