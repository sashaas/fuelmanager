namespace FuelManager.DataAccess.Migrations
{
	using System;
	using System.Data.Entity.Migrations;

	public partial class SummaryReport : DbMigration
	{
		public override void Up()
		{
			CreateTable(
				"dbo.SummaryReports",
				c => new
					{
						Id = c.Int(nullable: false, identity: true),
						Content = c.String(nullable: false),
						IP = c.String(),
						ReportOwner_Id = c.Int(nullable: false),
					})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.Users", t => t.ReportOwner_Id, cascadeDelete: true)
				.Index(t => t.ReportOwner_Id);

		}

		public override void Down()
		{
			DropForeignKey("dbo.SummaryReports", "ReportOwner_Id", "dbo.Users");
			DropIndex("dbo.SummaryReports", new[] { "ReportOwner_Id" });
			DropTable("dbo.SummaryReports");
		}
	}
}
