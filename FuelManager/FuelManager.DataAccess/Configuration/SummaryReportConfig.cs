﻿using FuelManager.Common;
using System.Data.Entity.ModelConfiguration;

namespace FuelManager.DataAccess.Configuration
{
	public sealed class SummaryReportConfig : EntityTypeConfiguration<SummaryReport>
	{
		public SummaryReportConfig()
		{
			ToTable("SummaryReports");

			Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

			Property(x => x.Content).IsRequired();

			HasRequired(f => f.ReportOwner).WithMany(x => x.SummaryReports).WillCascadeOnDelete(true);
		}
	}
}
