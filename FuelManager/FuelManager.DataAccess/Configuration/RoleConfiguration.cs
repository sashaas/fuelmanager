﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Configuration
{
	public class RoleConfiguration : EntityTypeConfiguration<Role>
	{
		public RoleConfiguration()
		{
			ToTable("Roles");
			HasKey(x => x.Id);
			Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

			Property(x => x.Name).IsRequired().HasMaxLength(30);
			Property(x => x.Description).IsOptional();

			HasMany(r => r.Users).WithMany(u => u.Roles).Map(
			m =>
			{
				m.MapLeftKey("RoleId");
				m.MapRightKey("UserId");
				m.ToTable("UserRoles");
			});
		}
	}
}
