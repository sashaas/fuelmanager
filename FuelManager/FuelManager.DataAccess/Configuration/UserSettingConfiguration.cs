﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Configuration
{
	public class UserSettingConfiguration : EntityTypeConfiguration<UserSettings>
	{
		public UserSettingConfiguration()
		{
			ToTable("UserSettings");

			HasKey(x => x.Id);
			HasRequired(x => x.Owner).WithOptional(x => x.Settings);

			HasRequired(x => x.SelctedCurrency).WithMany(x => x.SettingsForCurrency).HasForeignKey(x => x.CurrencyId);
		}
	}
}
