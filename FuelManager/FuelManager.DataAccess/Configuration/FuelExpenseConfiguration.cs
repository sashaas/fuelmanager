﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Configuration
{
	public class FuelExpenseConfiguration:EntityTypeConfiguration<FuelExpense>
	{
		public FuelExpenseConfiguration()
		{
			ToTable("FuelExpense");

			HasKey(x => x.Id);
			Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

			HasRequired(f => f.Currency).WithMany(c => c.Expenses).HasForeignKey(f => f.CurrencyTypeId);
		}
	}
}
