﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Configuration
{
	public class CurrencyTypeConfiguration : EntityTypeConfiguration<CurrencyType>
	{
		public CurrencyTypeConfiguration()
		{
			ToTable("CurrencyType");

			HasKey(x => x.Id);
			Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

			Property(x => x.Name).IsRequired();

			Property(x => x.Country).IsOptional();
			Property(x => x.Description).IsOptional();
		}
	}
}
