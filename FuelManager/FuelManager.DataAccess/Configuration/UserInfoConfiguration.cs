﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Configuration
{
	internal class UserInfoConfiguration : EntityTypeConfiguration<UserInfo>
	{
		public UserInfoConfiguration()
		{
			ToTable("UserInfos");

			HasKey(x => x.Id);
			Property(x => x.FirstName).IsOptional();
			Property(x => x.LastName).IsOptional();
			Property(x => x.Address).IsOptional();

			HasRequired(t => t.User).WithRequiredDependent(u => u.UserInfo);
		}
	}
}
