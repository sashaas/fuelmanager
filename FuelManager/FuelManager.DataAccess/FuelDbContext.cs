﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess
{
	public class FuelDbContext : DbContext
	{
		public FuelDbContext() :
			this("FuelCtx")
		{
		}

		public FuelDbContext(string conString) :
			base(conString)
		{
		}

		public DbSet<User> Users { get; set; }
		public DbSet<UserInfo> UserInfos { get; set; }
		public DbSet<Role> Roles { get; set; }

		public DbSet<CurrencyType> Currency { get; set; }
		public DbSet<UserSettings> UserSettings { get; set; }
		public DbSet<FuelExpense> FuelExpense { get; set; }

		public DbSet<SummaryReport> Reports { get; set; }


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();

			modelBuilder.Configurations.AddFromAssembly(typeof(FuelDbContext).Assembly);

			base.OnModelCreating(modelBuilder);
		}
	}
}
